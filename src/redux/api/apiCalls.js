import axios from 'axios'

const testServerBaseUrl = 'https://q2m-be-ssr.herokuapp.com/api/'
const prodctionApi = 'http://Network-LB-19c1c2f50ef59755.elb.us-east-1.amazonaws.com/api'

axios.defaults.baseURL = testServerBaseUrl;// switch / dev / test / porduction

const getToken = () => localStorage.getItem('token')

export const logInApi = async (payload) => await axios.post(`/user/login`, {
    ...payload
})

export const registerApi = async (payload) => await axios.post(`/user/register/`, {
    ...payload
})

export const saveFileApi = async (payload) => await axios.post('/dom/save',
    { ...payload },
    {
        headers: { "auth-token": getToken() },
    }
)

export const getAllFilesApi = async () => await axios.get('/dom', {
    headers: { "auth-token": getToken() },
})

export const deleteFileApi = async (id) => await axios.delete(`/dom/delete/${id}`, {
    headers: { "auth-token": getToken() }
})

export const updateFileApi = async (id, payload) => await axios.put(`/dom/update/${id}`,
    { ...payload },
    {
        headers: { "auth-token": getToken() }
    })